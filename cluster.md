# Cluster notes 
## Local cluster of ISTerre - IST-OAR
The local cluster of ISTerre will be used for testing and perfoming light simulations, in terms of computational power and memory required.

Visit:
 - https://isterre-intranet.osug.fr/spip.php?rubrique87
 - https://isterre-intranet.osug.fr/spip.php?article198

It uses the cluster managment system OAR. 

Documentation of OAR is available at
 - https://oar.imag.fr/
 - https://oar.readthedocs.io/en/2.5/

The cluster is quite heterogeneus, meaning every nodes has different type of processors. 
The interconnection between the nodes is poor. The parallelisation run well inside the single node.


### Interactive jobs
In a cluster computing environment, an interactive job refers to a type of computational task where a user interacts directly with the computing resources in real-time. 
Unlike batch jobs, which are executed without user intervention, interactive jobs allow users to interact with the computing resources while the job is running.

Use interactive jobs for 
- pre and post-processing simulations
- testing simulations setup and code
- running code that needs interaction

It is convinient to add the following aliases to `/home/$USER/.bashrc` file in the login home.
```bash
alias ij1="oarsub -I --project iste-equ-geodynamo -l /nodes=1/core=1,walltime=12:00:00" 

# interactive jobs in a specific node of the geodynamo team
alias ij1c26='oarsub -I --project iste-equ-geodynamo -l /nodes=1/core=1,walltime=4:00:00 -p "network_address='"'ist-calcul26.u-ga.fr'"'"'
alias ij1c8='oarsub -I --project iste-equ-geodynamo -l /nodes=1/core=1,walltime=4:00:00 -p "network_address='"'ist-calcul8.u-ga.fr'"'"'
alias ij1c3='oarsub -I --project iste-equ-geodynamo -l /nodes=1/core=1,walltime=4:00:00 -p "network_address='"'ist-calcul3.u-ga.fr'"'"'

# interactive job with 2 cores for 24 h
alias ij2t24="oarsub -I --project iste-equ-geodynamo -l /nodes=1/core=2,walltime=24:00:00"
```

### Batch jobs
```bash
alias jo="oarsub -S  ./oar.isterre"  # submit a batch job to a generic node
# submit a batch to specific nodes:
alias jo26='oarsub -S  ./oar.isterre -p "network_address='"'ist-calcul26.u-ga.fr'"'"'
alias jo8='oarsub -S  ./oar.isterre -p "network_address='"'ist-calcul8.u-ga.fr'"'"'
alias jo3='oarsub -S  ./oar.isterre -p "network_address='"'ist-calcul3.u-ga.fr'"'"'
alias jt="oarsub -S --project iste-equ-geodynamo oar.isterre -p" dedicated='none'" -t devel"
```

When a job is done, informations are written in certain files:    
 - `OAR.JOB_ID.stdout` -> output of the job   
 - `OAR.JOB_ID.stderr` -> errors of the job   

```bash
oardel 'job_id' # To remove a job from the queue
```

### Cluster status
The following aliases are for investigate the state of your simulations:
```bash
alias qq="oarstat -u $USER"  # simulations status
alias qL="oarstat -u $USER -f | grep -e Job -e launchingDirectory | sed 's/Job_Id: //' | sed 's/ launchingDirectory = //'"  # simulations folder
```
To investigate the cluster usage state: `chandler`.

### PATH and source 
```bash
export PATH="/data/geodynamo/$USER/src/:$PATH"
export PATH="/home/$USER/.local/bin/:$PATH"
```
### storage IST-OAR
Visit https://isterre-intranet.osug.fr/spip.php?article200.

There are two possibilities for storage 
    - `/data/geodynamo/$USER` - data are backed up (solver, simulation data)
    - `/nfs_scratch/$USER` - faster access, but not backed up (temporary simulations)


### installation of XSHELLS for cpu
```bash
module purge
module purge
module load intel-devel/18.0.1
module load python
```

