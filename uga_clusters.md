# University clusters - UGA
There are multiple clusters at UGA. 
- DAHU - HPC on CPU and data analysis
- BIGFOOT - HPC on GPU
All the details are collected at https://gricad-doc.univ-grenoble-alpes.fr/hpc/ .
Read it carefully before using these clusters.

## bash shortcut on the local machine
```bash
project_name="your_project_name"
alias dahu='ssh dahu.ciment'  # connect to CPU cluster
alias bigfoot='ssh bigfoot.ciment'  # connect to GPU cluster
alias bettikG='sshfs $USER@cargo.univ-grenoble-alpes.fr:/bettik/PROJECTS/$project_name/$USER/ ~/net/bettik/PROJECTS/$project_name/$USER'
```
## bash shortcut on the cluster login 

```bash                                                                                    
alias jo="oarsub -S  ./oar.dahu" # submit the passive job

alias qq="oarstat -u $USER"  # simulations status                                       
alias qL="oarstat -u $USER -f | grep -e Job -e launchingDirectory | sed 's/Job_Id: //' | sed 's/ launchingDirectory = //'"  # simulations folder

```                                                           

Then, we only need to change the #OAR parameters in the /oar.dahu file.

```bash
#OAR -n job                                #name of the job
#OAR --project $project_name              #name of the project
#OAR -l /nodes=1/core=16,walltime=8:00:00  #cores/walltime to allocate to the cluster
```   

To investigate the cluster usage state: `chandler`. 


### Copy a folder from one cluster to other one

Add `config` in .ssh of oar (only the first time)

```bash
vi ~/.ssh/config

                                                                                               
Host *
  ServerAliveInterval 30

Host *.ciment
  User $USER
  ProxyCommand ssh -q $USER@access-gricad.univ-grenoble-alpes.fr "nc -w 60 `basename %h .ciment` %p"

```

Then copy your ssh-id from local machine to dahu (only the first time)

```bash
ssh-copy-id dahu.ciment
```

In the folder that you want to copy, copy with `scp`
```bash
scp -r folder_to_copy $USER@cargo.univ-grenoble-alpes.fr:/bettik/PROJECTS/$project_name/$USER/folder_destination

```
Or, with the `rsync` command (cleaner than the cp command)
```bash
rsync -av path_to_files_to_copy/ path_where_to_copy/

```
### For loops on the bash

This loop for can be done only on the cluster, from a folder that contains a similar name, goes inside each folder and execudte the job 



### XSHELLS installation on cpu
Installation is done in the login enviroment, using conda as a package manager
for the dependences.
```bash
source /applis/environments/conda.sh
conda info --envs
conda activate xshells-cpu
conda list
conda install -c conda-forge c-compiler compilers cxx-compiler
conda install -c conda-forge fftw
conda install -c conda-forge gnuplot
conda install -c conda-forge openmpi
cd /bettik/PROJECTS/$project_name/$USER/src/xshells_/
git clone --recurse-submodules git@gricad-gitlab.univ-grenoble-alpes.fr:schaeffn/xshells.git
PYTHON=/home/$USER/.conda/envs/xshells-cpu/bin/python
MPICXX=/home/$USER/.conda/envs/xshells-cpu/bin/mpicxx
cd xshells
./configure
make xsbig
make xsbig_mpi
```
### XSHELLS installation on gpu
Currently not working.
```bash
bigfoot # cluster for GPU
source /applis/environments/conda.sh
conda activate GPU
# to list the cuda version availables
# source /applis/environments/cuda_env.sh bigfoot -l
source /applis/environments/cuda_env.sh bigfoot 11.7
conda install -c conda-forge gnuplot
conda install -c conda-forge fftw
```
