# Vim 
**Vim**, short for "Vi Improved," is a highly customizable and powerful text editor, renowned for its efficiency and versatility. 
It offers two main modes: command mode for navigating and executing commands, and insert mode for entering text. 
To install Vim on Ubuntu, open a terminal and type `sudo apt-get update` to ensure you have the latest package information. 
Then, type `sudo apt-get install vim` to install Vim.
Once installed, you can access Vim by typing `vim` in the terminal. 
One notable advantage of Vim is its ubiquity across computing environments.
It's often pre-installed on Unix-like systems, including Ubuntu, and is readily available on remote clusters, making it a go-to choice for developers and system administrators working across different platforms. Its availability on clusters ensures consistent and familiar editing capabilities, empowering users to efficiently manage and manipulate text files in various computing environments.

# Tutorial/Commands to use Vi/Vim    
In *vi* or *vim*, there are two modes to consider:

**Command Mode:** Default mode upon opening a file.

- `i`: Enter insert mode.
- `I`: Enter insert mode at the start of the line.
- `a`: Enter insert mode, one character after the cursor.
- `A`: Enter insert mode at the end of the line.
- `o`: Enter insert mode below the line where the cursor is.
- `O`: Enter insert mode above the line where the cursor is.
- `Esc`: Exit insert mode (enters command mode).

**Command Mode Commands:**

- `:w`: Save the file.
- `:q`: Quit the file (will display a warning if the file was not saved after changes).
- `:wq`: Save the file and quit.
- `:q!`: Quit the file WITHOUT saving.
- `0`: Go to the start of the line.
- `$`: Go to the end of the line.
- `up/k`: Go to the line above.
- `down/j`: Go to the line below.
- `gg`: Go to the first line.
- `G`: Go to the last line.
- `yy`: Copy the line where the cursor is.
- `p`: Paste below the cursor.
- `P`: Paste above the cursor.

# Cheat Sheet and other useful links                                              
- https://github.com/iggredible/Learn-Vim 
- https://learnxinyminutes.com/docs/vim/
- https://vim.rtorr.com/
- https://vimsheet.com/
- https://github.com/sk3pp3r/cheat-sheet-pdf/blob/master/pdf/vim-cheat-sheet.pdf
- https://devhints.io/vim
