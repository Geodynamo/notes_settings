# Commands and shortcuts for Unix shells
## bash
Bash is a UNIX shell, it is the command-line interface for both local Linux machines and cluster environments.

## Online learning resources for bash                                                                       
- https://learnxinyminutes.com/docs/bash/                                               
- https://swcarpentry.github.io/shell-novice/                                           
- https://carpentries-incubator.github.io/shell-extrasV/     
- https://www.freecodecamp.org/news/bash-scripting-tutorial-linux-shell-script-and-command-line-for-beginners/

## Aliases
Aliases in the .bashrc file are a convenient way to create shortcuts for commands or command sequences you use frequently. They essentially allow you to create your own custom commands or abbreviations for longer commands. Here's how you can use aliases in the .bashrc file:
1. Open your .bashrc file in a text editor. This file is typically located in your home directory (~/.bashrc).
2. Scroll to the bottom of the file or wherever you prefer to add your aliases.
3. To create an alias, use the following syntax:
```bash
alias alias_name='command'
```
Replace alias_name with the name you want to use for your alias, and replace command with the actual command or command sequence you want to abbreviate.
4. Save the .bashrc file and exit the text editor.
5. To apply the changes, you can either restart your terminal session or run the following command in your terminal: `source ~/.bashrc`

Example of possible aliases:
```bash
alias ls='ls -la'
alias :q='exit'                                                                         
alias oo='xdg-open'                                                                     
alias vi='vim'                                                                          
alias mkdir='mkdir -p'       # Create parent directories if they don't exist
alias df='df -h'             # Disk usage (human-readable)
alias du='du -h'             # Directory usage (human-readable)
```
## PATH variable
The PATH variable plays a crucial role in shell environments like Bash. 
It's essentially a list of directories that the shell searches through to find executable files. 
When you type a command in the terminal, the shell looks through each directory listed in the PATH variable for an executable file with the same name as the command you entered.
If the command is found in one of the directories listed in the PATH, the shell executes it. If not, you'll receive a "command not found" error.
When you add custom scripts or executables to your system, you often need to update the PATH variable to include the directory where those scripts or executables are located. 
This ensures that you can run them from any directory without specifying their full path.
You can view the current value of the PATH variable by running the following command in your terminal: `echo $PATH`
To add a directory to the PATH, you can modify the .bashrc file and add a line like this:
```bash
export PATH="$PATH:/path/to/your/directory"
```

## Overview on some useful shell command
In the following paragraph, some of the most 
- To edit a document using the terminal you can use the following command, using `file.txt` as example
```bash
gedit file.txt   # text editor on graphical user interface
nano file.txt    # text editor over the terminal 
vi file.txt      # text editor over the terminal 
```
The first command opens a text editor where we are able to edit and save the file, the other two commands opens the allow the file to be read and edited directly on the terminal.


 - To just read the document, without modifying it, you can use the commands
 
 
```bash
more file.txt # scroll in the file
cat file.txt # show the full file
```

 - There are two commands to read the first and last 42 lines of a file:
 
```bash
head -n 42 file.txt
tail -n 42 file.txt
```

The command with no argument (`head file.txt`) is showing only the first 10 lines, similarly for tail (last 10 lines). 
This command is used to 

 - To observe the difference between 2 files (in the same folder) and see where are the difference use
 
```bash
diff file.txt file1.txt
```
 to compare to file from different folders use
 
```bash
diff file.txt ~/folder1/folder2/file1.txt
```

 - To eliminate one or many files use (*be careful* they are deleted forever, there is no trash bin)

```bash
rm -v file.txt file1.txt
rm -v *.txt  # eliminate all files that end with .txt
rm -v file*  # eliminate all files that start with file
```
 
- To create a new folder in a specific path 

```bash
mkdir -p my_path/a/b/c/folder 
```

 - To search a specific word in a file use
```
grep word file.txt
grep 'A=2' file.txt # if there is special simbols we use ''
```
- Other useful command:
  - `ls` List directory contents.
  - `cd` Change directory.
  - `pwd` Print working directory.
  - `mkdir` Make directory.
  - `rm` Remove files or directories.
  - `cp` Copy files or directories.
  - `mv` Move or rename files or directories.
  - `touch` Create an empty file.
  - `cat` Concatenate and display files.
  - `grep` Search for patterns in files.
  - `chmod` Change file permissions.
  - `chown` Change file ownership.
  - `man` Display the manual for a command.
  - `echo` Print arguments to the standard output.
  - `wc` Count words, lines, and characters in a file.
  - `find` Search for files and directories.


## zsh
Zsh, short for Z Shell, is a powerful shell program for Unix-like operating systems. 
It's an interactive shell with many advanced features, including customizable prompts, spelling correction, filename globbing, and advanced scripting capabilities. 
Zsh is known for its extensibility through plugins and themes, making it a popular choice among power users and developers.

You can use `zsh` as default shell environment in the local machine.
To install it on ubuntu:
```bash
sudo apt update
sudo apt install zsh
```
In order to have a nicer setup we use the following configuration settings script (https://ohmyz.sh/)
to install it (check on the previous website that it is still the right command)
```
`sh -c "$(wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"`
```
### hash directories
One of the advantages of `zsh` is the `hash -d` function.
If you add the following line to your `.zshrc` file:
```zsh
hash -d fold3=~/folder1/folder2/folder3
```
it is possible to now access `~/folder1/folder2/folder3` simply by typing `~fold3`. 
This can save you a lot of time and typing, especially for frequently accessed directories buried deep within your file system.
A better explanation at https://til.hashrocket.com/posts/xsavbhlrz4-shortcuts-with-hash-d-in-zsh .

### alias for extensions
The alias -s command in Bash allows you to define aliases for specific file extensions. 
This feature lets you create custom commands that automatically open files of a particular type with a specified program.
```bash
alias -s extension='command'
```
Just the file name on the terminal leads to automatically open the file in the desired program.
Example:
```bash
alias -s pdf='evince'                                                                   
alias -s png='eog'                                                                      
alias -s jpg='eog'                                                                      
alias -s svg='eog'                                                                      
alias -s mp4='vlc'                                                                      
alias -s py='vim'                                                                      
alias -s par='vim'                                                                     
alias -s txt='vim' 
```

# More advanced command in shell
## Loops in bash 
The *for loop* iterates over a sequence of values or items. Here's its basic syntax:
```bash
#!/bin/bash
for i in {1..5}   # for loop, variable i from 1 to 5
do                # what the loop needs to do
    echo "$i"     # to call the variable i, we use $
done              # when to stop the loop
```
In order to iterate across subfolders:
```bash
#!/bin/bash
for folder in name*
do
  # Commands to execute
  cd $folder || exit  # enter in the folder 
  ls -lh              # execute the command 
  cd ..               # exit the folder
done
```
The *while loop* repeats a block of code as long as a specified condition is true. Its syntax is:
```bash
#!/bin/bash
count=5
while [ $count -gt 0 ]
do
    echo $count
    count=$((count - 1))
done
```
Here, `[ $count -gt 0 ]` checks if the value of count is greater than 0, and the loop continues as long as this condition is true. Inside the loop, count is decremented by 1 with each iteration.

## Redirecting output stream
In Bash, the `>` symbol is used for output redirection. It's a powerful feature that allows you to control where the output of a command goes. Here's how it works:
*Redirecting Output to a File:* You can use `>` to redirect the standard output (stdout) of a command to a file. For example:
```bash
command > output.txt
```
This command will run `command` and write its output to a file named `output.txt`. 
If the file doesn't exist, it will be created. If it does exist, its contents will be overwritten.

*Appending Output to a File:* If you want to append the output to a file rather than overwriting it, you can use `>>` like this:

```bash
command >> output.txt
```
This will append the output of `command` to the end of the file `output.txt`.

## Piping
In Bash, the `|` symbol, called a pipe, is used to connect the standard output (stdout) of one command to the standard input (stdin) of another command. 
This enables you to create chains of commands, where the output of one command becomes the input for another, allowing for powerful data processing and manipulation.
Here's how piping works:
You can chain multiple commands together with pipes, creating complex data processing pipelines. For example:
```bash
command1 | command2 | command3
```
Here, the output of `command1` is piped into `command2`, and the output of `command2` is piped into command3.

Data Flow: When commands are piped together, each command processes the data it receives from the previous command's output. 
This allows for efficient and concise data manipulation.

Example: 
Let's say you have a text file called `data.txt containing a list of names, one per line. 
You want to count the number of lines and then display the result. 
You can do this using piping:
```bash
cat data.txt | wc -l
```
Here, cat `data.txt` outputs the contents of `data.txt`, which is then piped into `wc -l` (word count with the -l option), which counts the number of lines in the input.
Combining with Redirection: Piping can also be combined with output redirection. For example:
```bash
command1 | command2 > output.text
```
This takes the output of `command1`, feeds it into `command2`, and then redirects the output of `command2` to a file named `output.txt`.

## Terminal multiplexers
### GNU Screen
GNU Screen is a terminal multiplexer program for Unix-like operating systems, a command-line application that provides a virtual terminal session manager
It allows users to create multiple terminal sessions within a single window or to detach and reattach sessions, making it especially useful for managing multiple tasks or sessions on remote servers.

It enables users to run multiple terminal sessions simultaneously, even if they are logged in from different locations.
- *Session Persistence:* Users can detach from a session, allowing processes to continue running in the background, and later reattach to the same session from another location or after logging out.
- *Multi-Tasking*: Screen allows users to split their terminal window into multiple regions, each running its own shell or application. This enables efficient multitasking.
- *Remote Access*: It's particularly useful for remote administration of cluster or long-running tasks. Even if the connection is lost, the session continues running, and users can reconnect later to check on progress or make adjustments.

*Relevant Commands*:
 - `screen`: This command initiates a new Screen session.
 - `screen ls`: Lists all available Screen sessions.
 - `screen -r [session_name]` or `screen -r [session_ID]` : Reattaches to a detached Screen session, either by specifying its name or its unique session ID.

*Detach from the Session:* This is the most common way to leave a TMUX session without terminating it entirely. Press the key combination Ctrl + a, then release both keys and press d. This will detach you from the session, allowing it to continue running in the background.

### TMUX
TMUX is a terminal multiplexer for Unix-like operating systems that enables users to create and manage multiple terminal sessions within a single window. 
It's designed to be more modern and feature-rich compared to GNU Screen.
TMUX offers several advantages:
- *Session Management:* Like GNU Screen, TMUX allows users to create multiple terminal sessions and detach from them, letting processes continue running in the background. Users can later reattach to these sessions, even after logging out or disconnecting.
- *Window Splitting:* TMUX provides more flexible window splitting options, allowing users to divide a single terminal window into multiple panes horizontally and vertically. This feature facilitates efficient multitasking and simultaneous monitoring of multiple processes.
- *Customization:* TMUX is highly customizable, allowing users to configure key bindings, customize status bars, and create complex layouts tailored to their workflow.
- *Scripting Support:* TMUX offers a rich set of commands and scripting capabilities, enabling automation and integration with other tools and workflows.

*Relevant Commands*:
- `tmux`: Initiates a new TMUX session.
- `tmux ls`: Lists all available TMUX sessions.
- `tmux attach -t [session_name]` or `tmux attach -t [session_ID]`: Reattaches to a detached TMUX session, either by specifying its name or its unique session ID.
- `tmux new-window`: Creates a new window within the TMUX session.
- `tmux split-window`: Splits the current window into multiple panes.

*Detach from the Session:* This is the most common way to leave a TMUX session without terminating it entirely. Press the key combination Ctrl + b, then release both keys and press d. This will detach you from the session, allowing it to continue running in the background.
