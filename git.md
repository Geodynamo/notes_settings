# git
Git is a widely used distributed version control system designed for tracking changes in source code during software development. 
It allows multiple developers to collaborate on projects efficiently by managing a history of changes and facilitating teamwork through features like branching and merging. 
With Git, developers can work on different aspects of a project simultaneously, experiment with new ideas without disrupting the main codebase, and easily revert to previous states if needed. 
It's a versatile tool that provides a robust framework for managing code and ensuring project integrity. 

## Online resources
- https://learnxinyminutes.com/docs/git/
- https://swcarpentry.github.io/git-novice/

## .gitignore

A .gitignore file is a text file used by Git to specify intentionally untracked files that Git should ignore. These files are typically ones that you don't want to be included in your Git repository, such as *temporary files*, binaries, or sensitive information like passwords. By listing these files or directories in the .gitignore file, you ensure that they won't be accidentally committed to your repository, keeping your repository clean and focused on the essential code and resources. The .gitignore file uses simple pattern matching rules to specify which files or directories to ignore, allowing for flexibility in defining what should be excluded from version control. 

You can generate a ´.gitignore´ file from the following link
 - https://www.toptal.com/developers/gitignore

Or copy the file [.gitgnore](/.gitignore)

## git commands

```
git clone ssh:xxx   # To create a local clone repository
git status          # To see changed files
git add             # Add files from the status
git rm              # Remove files from the status
git commit -m       # Commit the changes
git log --graph     # view the differents commit branches as a graph
git push -u         # push the commit onto gitlab
git pull origin     # update the local repository from the online one
```

## Git LFS
Git LFS (Large File Storage) is an open-source extension for Git that deals with large files by storing them outside the Git repository, while still keeping track of their changes and versions. It's particularly useful for managing files like images, videos, datasets, and other binary files that are too large to be efficiently handled by Git alone. Instead of storing the actual file content in the Git repository, Git LFS replaces them with pointers or references, and the actual file content is stored on a separate server. This helps keep the repository size manageable and speeds up operations like cloning and fetching. 

1. **Install Git LFS**: First, you need to install Git LFS on your system. You can download the installer from the [Git LFS GitHub repository](https://github.com/git-lfs/git-lfs/releases) or use a package manager if available for your platform.

2. **Initialize Git LFS in your repository**: Navigate to your Git repository directory using the command line and run the following command to initialize Git LFS for that repository:
```
git lfs install
```

3. **Track large files**: Identify the types of files you want to track with Git LFS. These could be binary files, large media assets, datasets, etc. Once you know which files you want to track, use the `git lfs track` command to start tracking them. For example:
```
git lfs track ".jpg"
git lfs track ".mp4"
git lfs track ".pdf"
```
Make sure .gitattributes is tracked:
```
git add .gitattributes
```
4. **Commit and push**: After tracking the large files, commit your changes as usual and push them to the remote repository. Git LFS will manage the large files according to the tracking rules you've set up.


