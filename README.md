# Notes and settings for students working in the GEODYNAMO team

Welcome to the team! The following pages contain information about IT operations.

These notes provide details on useful settings and commands.

They are organized into separate files to enhance clarity and accessibility.

If you use a cluster, you can refer to the following pages:
 - bash: [bash.md](/bash.md) 
 - isterre (local) cluster: [cluster.md](/cluster.md) 
 - uga (university) cluster: [uga_cluster.md](/uga_cluster.md)
 - national clusters: [national_cluster.md](/national_cluster.md)
 - ssh and vpn: [ssh_vpn.md](/ssh_vpn.md)
 - vim: [vim.md](/vim.md)

If you are coding, the following pages can be of interest:
 - git: [git.md](/git.md) 
 - python: [python.md](/python.md) 
 - vim: [vim.md](/vim.md)
 - YAML: [YAML.md](/YAML.md) 

If you are running simulations, check out this resources:
 - xshells: [xshells.md](/xshells.md)
 - COMSOL: [comsol.md](/comsol.md)

For good IT practices in scientific computing, refer to the following papers:
 - Wilson et al., Good enough practices in scientific computing, https://doi.org/10.1371/journal.pcbi.1005510
 - Wilson et al., Best Practices for Scientific Computing, https://doi.org/10.1371/journal.pbio.1001745

Currently, this set of notes is a *work in progress*. 
Feel free to contribute additional information as needed.


