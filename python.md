# Python
We use python for pre-post processing, and several other tasks.
## Useful links:
- https://learnxinyminutes.com/docs/python/
- https://swcarpentry.github.io/python-novice-gapminder/
- https://swcarpentry.github.io/python-novice-inflammation/
- https://carpentries-incubator.github.io/swc-ext-python/
## Libraries
We use the following libraries:
 - numpy: linear algebra
 - scipy: scientific computing
 - matplotlib: plotting
 - xarray: data handling
 - shtns: spherical harmonics
## Conda and enviroments
## Editor
You can use the editor that you prefer.
If everything is very new, or if you had some experience with MATLAB, spyder is
a good candidate, because it includes editor, REPL and variable explorer in the 
same interface.
You can install spyder via conda.

