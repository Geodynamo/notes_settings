# Xshells Installation and setup

To get the repository for xshells :

```
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/schaeffn/xshells.git
```

To setup xshells :

```
./setup
```

If needed to install the fftw3 library :

```
sudo apt install libfftw3-dev
```

If needed during the setup and the installation of the python modules, we can do :

```
sudo apt install python3-numpy  (or any other library if the library is not found)
```

# Compiling files

So now we can try to copy .par and .hpp files from a problem in the xshells/problems/ folder by using :

```
cp problems/libration/* .
```

and compiling in the xshells directory. 


# Installing Anaconda to use Spyder


To activate conda :

```
eval "$(/home/thomasguerry/anaconda3/bin/conda shell.bash hook)"
```

To install spyder :

```
conda install spyder
```

# Xsbig, Xsplot, Xspp and bashrc file.

In the xshells folder, we type :

```
make xsbig
make xspp
```

depending on the xshells.par and xshells.hpp files of our problem. To use xsplot, we edit the bashrc file to include the path to the xsplot executable, to be able to run the command xsplot anywhere. In the bashrc file :

```
export PATH="$PATH:/~/Directory"
```

We can use Xsplot to plot 1D or 2D data, and Xspp to extract slices, profiles, spectra from the fields.





