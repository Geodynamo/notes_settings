# Numerical architecture notes and ssh commands notes.   
   
Personnal Machine -> Vpn  host ->    Fronthost    ->   Cluster   
     ist-XXX      ->  ist-ssh  -> ist-oar.u-ga.fr -> ist-calculX (3,8,26 for the geodynamo team)   
   
Generating a ssh key:   
   
```   
ssh-keygen -t ed25519   
```
   
Each generating command, generates a private and public key, only copy the public one.   
To copy the newly generated key onto a connecting system:   
   
```   
ssh-copy-id ist-XXX   
```    
   
We need a passphrase for each generated key to authentify us onto the connecting system.   
   
ssh commands:   
   
```   
ssh -X ist-XXX   
ssh -X ist-ssh -t ssh -X ist-oar #To connect to the cluster through the vpn   
```   
   
In the .bashrc file:   
   
```   
alias ist='ssh -X $USER@ist-oar.u-ga.fr'   
alias istT='ssh -X ist-ssh -t ssh -X ist-oar'   
```   

