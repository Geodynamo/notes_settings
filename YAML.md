# YAML
YAML (YAML Ain't Markup Language) is a human-readable data serialization format commonly used for configuration files and data exchange. 
It's designed to be easily readable by both humans and machines. 
YAML uses indentation to represent data hierarchy and supports various data types such as strings, integers, lists, and dictionaries. In Python, you can use the PyYAML library to work with YAML files. 
Here's a simple example demonstrating how to read from and write to a YAML file using PyYAML:

```python
import yaml

# Example dictionary data
data = {
    'name': 'precession',
    'nu': 1e-6,
    'eta': [1e-1, 1e-2, 1e-3]
    'color': ['b', 'k', 'g']
}

# Writing data to a YAML file
with open('data.yaml', 'w') as file:
    yaml.dump(data, file)

# Reading data from the YAML file
with open('data.yaml', 'r') as file:
    loaded_data = yaml.safe_load(file)

print("Loaded data from YAML file:")
print(loaded_data)
```

We use YAML for configuartion files and other database-like operation.
It is very simple to use it and can be parsed automatically by different
programming languages.

## Useful links:
- https://learnxinyminutes.com/docs/yaml/
- https://yaml.org/
