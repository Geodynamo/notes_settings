# National computer: Jean Zay
Jean-Zay documentaton is at http://www.idris.fr/eng/jean-zay/
Cheat sheet is at http://www.idris.fr/jean-zay/cheat-sheet.html

## To set up XSHELLS
- Log into Jean-zay via ssh
- get xshells from git and checkout specific working version
- put files in $WORK directory
```bash
cd $WORK
git clone --recurse-submodules https://bitbucket.org/nschaeff/xshells.git
git checkout 7af6bf6937fc69d7c59cd90913c33aa82b07ef74
```

## Start an interactive job
- assign 10 cpus, 1 gpu
```bash
#find project name
idrproj
#start interactive job
srun --pty --nodes=1 --ntasks-per-node=1 --cpus-per-task=10 --gres=gpu:1 --hint=nomultithread --account=<project_name>@v100 bash
```

## Configure XSHELLS
- change prefix to $WORK (or wherever else you want the files)
```bash
cd xshells
module load fftw 
./configure --enable-cuda --prefix=$WORK/src
cp /projects/dbbldiff/xshells.par .
cp /projects/dbbldiff/xshells.hpp .
make xsgpu
./xsgpu
```

