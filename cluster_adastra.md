## Adastra

Quick start: https://dci.dci-gitlab.cines.fr/webextranet/quick_start/index.html

In home, symbolically link the relevant folders
```
ln -s $WORKDIR work 
```

Transfer file from local cluster to national cluster:
```
rsync -av folder_on_ist_oar your_adastra_user_name@adastra.cines.fr:folder_on_adastra
```

